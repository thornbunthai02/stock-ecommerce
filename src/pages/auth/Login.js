import {
  Box,
  Container,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import background from "../../assets/images/background1.png";
import { LoadingButton } from "@mui/lab";
import { useNavigate } from "react-router-dom";
import { DASHBOARD_PATH } from "../../routes/path";

function Login() {
  const navigate = useNavigate();
  const handleSubmit = () => {
    navigate(DASHBOARD_PATH.main);
  };
  return (
    <Container
      maxWidth={"lg"}
      sx={{
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}>
      <Grid container spacing={2} height={"100%"}>
        <Grid
          item
          md={6}
          sm={6}
          xs={0}
          sx={{
            flex: 1,
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            boxShadow: "rgb(186 186 186 / 15%) 2.4px 2.4px 3.2px",
          }}>
          <Box>
            <img
              style={{ width: "100%", height: "100%", objectFit: "contain" }}
              src={background}
              alt="background"
            />
          </Box>
        </Grid>
        <Grid
          item
          md={6}
          sm={6}
          xs={12}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Stack
            display={"flex"}
            alignItems={"center"}
            flexDirection={"column"}
            gap={2}
            sx={{ width: "60%" }}>
            <Box>
              <Typography>Welcome to Store</Typography>
            </Box>
            <Box
              sx={{
                width: "100%",
                display: "flex",
                gap: 2,
                flexDirection: "column",
              }}>
              <TextField
                fullWidth
                size="medium"
                label="Username"
                variant="outlined"
              />
              <TextField
                fullWidth
                size="medium"
                label="Password"
                variant="outlined"
              />
            </Box>
            <Box sx={{ width: "100%" }}>
              <LoadingButton
                color="primary"
                fullWidth
                variant="contained"
                onClick={handleSubmit}>
                Login
              </LoadingButton>
            </Box>
          </Stack>
        </Grid>
      </Grid>
    </Container>
  );
}

export default Login;
