import { DASHBOARD_PATH } from "../../routes/path";

export const NAV_DATA = [
  {
    title: "Managment",
    child: [
      {
        id: 1,
        name: "Dashboard",
        icon: "mdi:view-dashboard",
        url: DASHBOARD_PATH.main,
        child: [],
      },
      {
        id: 2,
        name: "Stock",
        icon: "mdi:store",
        child: [
          {
            id: 2.1,
            name: "Category",
            icon: "mdi:dot",
            url: DASHBOARD_PATH.category,
            child: [],
          },
          {
            id: 2.2,
            name: "Product",
            icon: "mdi:dot",
            url: DASHBOARD_PATH.product,
            child: [],
          },
        ],
      },
    ],
  },
];
