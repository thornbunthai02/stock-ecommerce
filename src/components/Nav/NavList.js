import * as React from "react";
import { styled } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import { Icon } from "@iconify/react";
import { drawerWidth } from "../..";
import { NAV_DATA } from "./nav-config";
import { Box, Collapse, ListSubheader } from "@mui/material";
import { isEmpty } from "lodash";
import { Link, matchPath, useLocation } from "react-router-dom";
import Logo from "../../assets/images/logo.png";

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});
export const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

function NavList({ open }) {
  return (
    <Drawer variant="permanent" open={open}>
      <DrawerHeader
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: 1,
          p: 2,
          backgroundColor: "#efefef",
          borderRadius: "3px",
        }}>
        <Box sx={{ width: open ? "25%" : "40%", height: "50px" }}>
          <img
            src={Logo}
            alt="Logo"
            style={{ width: "100%", height: "100%", objectFit: "contain" }}
          />
        </Box>
        <Typography sx={{ display: open ? "flex" : "none" }}>
          Stock Ecommerce
        </Typography>
      </DrawerHeader>
      {/* <Divider /> */}
      <>
        {NAV_DATA.map((item, index) => (
          <List
            key={item.id}
            subheader={
              <ListSubheader>
                <Typography variant="body2" sx={{ mt: 2, mb: 1 }}>
                  {item?.title}
                </Typography>
              </ListSubheader>
            }>
            {item.child.map((child) => (
              <NavItem key={child.id} child={child} openDrawer={open} />
            ))}
          </List>
        ))}
      </>
    </Drawer>
  );
}
export default NavList;

const NavItem = ({ child, openDrawer, depth = 2 }) => {
  const isActive = ActiveMenu(child.url);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(!open);
  };
  return (
    <>
      <ListItem
        component={isEmpty(child.child) ? Link : Box}
        to={child.url}
        key={child.id}
        disablePadding
        sx={{ display: "block" }}
        onClick={handleOpen}>
        <ListItemButton
          sx={{
            mx: 2,
            minHeight: 48,
            justifyContent: openDrawer ? "space-between" : "center",
            px: 2,
            backgroundColor: (theme) =>
              isActive ? theme.palette.primary.main : "",
            ...(isActive && {
              ":hover": {
                backgroundColor: (theme) =>
                  isActive ? theme.palette.primary.main : "",
              },
            }),
            borderRadius: "5px",
            pl: `${depth * 8}px`,
          }}>
          <Box sx={{ display: "flex", alignItems: "center", gap: 0.8 }}>
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: openDrawer ? 1.5 : "auto",
                justifyContent: "center",
              }}>
              <Icon
                width={18}
                icon={child.icon}
                color={isActive ? "#FFF" : "rgba(0, 0, 0, 0.54)"}
              />
            </ListItemIcon>
            <Typography
              variant="subtitle1"
              sx={{
                display: openDrawer ? "flex" : "none",
                color: (theme) => (isActive ? "#FFF" : "rgba(0, 0, 0, 0.54)"),
                fontSize: "14px",
              }}>
              {child.name}
            </Typography>
          </Box>

          {!isEmpty(child.child) && (
            <Icon
              width={20}
              icon={open ? "mdi:expand-less" : "mdi:expand-more"}
            />
          )}
        </ListItemButton>
      </ListItem>
      {!isEmpty(child.child) && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {child.child.map((item) => (
              <NavItem
                openDrawer={openDrawer}
                child={item}
                key={item.id}
                depth={depth + 1}
              />
            ))}
          </List>
        </Collapse>
      )}
    </>
  );
};

const ActiveMenu = (url) => {
  const { pathname } = useLocation();
  const isActive = url
    ? !!matchPath({ path: url, end: true }, pathname)
    : false;
  return isActive;
};
