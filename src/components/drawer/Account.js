import {
  Avatar,
  Box,
  Divider,
  IconButton,
  MenuItem,
  Popover,
  Stack,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { alpha } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";

function Account() {
  // const { enqueueSnackbar } = useSn();
  const navigate = useNavigate();

  const [openPopover, setOpenPopover] = useState(null);
  const open = Boolean(openPopover);
  const id = open ? "simple-popover" : undefined;

  const handleOpenPopover = (event) => {
    setOpenPopover(event.currentTarget);
  };

  const handleClosePopover = () => {
    setOpenPopover(null);
  };
  const handleLogout = () => {
    navigate("/");
  };
  return (
    <>
      <IconButton
        aria-describedby={id}
        onClick={handleOpenPopover}
        sx={{
          p: 0,
          ...(openPopover && {
            "&:before": {
              zIndex: 1,
              content: "''",
              width: "100%",
              height: "100%",
              borderRadius: "50%",
              position: "absolute",
              bgcolor: (theme) => alpha(theme.palette.grey[900], 0.8),
            },
          }),
        }}>
        <Avatar src={null} alt={null} name={null} />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={openPopover}
        onClose={handleClosePopover}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        // sx={{ width: 200, p: 0 }}
      >
        {/* <Box sx={{ my: 1.5, px: 2.5 }}>
          <Typography variant="subtitle2" noWrap>
            User
          </Typography>

          <Typography variant="body2" sx={{ color: "text.secondary" }} noWrap>
            {"Admin"}
          </Typography>
        </Box> */}

        {/* <Divider sx={{ borderStyle: "dashed" }} />

        <Stack sx={{ p: 1 }}>
          {OPTIONS.map((option) => (
            <MenuItem
              key={option.label}
              onClick={() => handleClickItem(option.linkTo)}>
              {option.label}
            </MenuItem>
          ))}
        </Stack> */}

        <Divider sx={{ borderStyle: "dashed" }} />

        <MenuItem onClick={handleLogout} sx={{ m: 1 }}>
          Logout
        </MenuItem>
      </Popover>
    </>
  );
}

export default Account;
