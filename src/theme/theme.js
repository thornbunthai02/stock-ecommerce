import { blue, grey, pink, purple } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

const primary = "#000";
const secondary = "#FFF";

export const theme = createTheme({
  //   spacing: 4,
  palette: {
    primary: {
      light: blue[300],
      main: purple[500],
      dark: blue[700],
    },
    secondary: {
      light: pink[500],
      main: "#11cb5f",
      dark: pink[700],
    },
  },
  //   header: {
  //     color: grey[500],
  //     background: "#FFF",
  //     search: {
  //       color: grey[800],
  //     },
  //     indicator: {
  //       background: secondary,
  //     },
  //   },
  //   sidebar: {
  //     width: 260,
  //     color: "#FFF",
  //     background: secondary,
  //     header: {
  //       color: "#FFF",
  //       background: secondary,
  //       brand: {
  //         color: "#3a4244",
  //       },
  //     },
  //     footer: {
  //       color: "#FFF",
  //       background: secondary,
  //       online: {
  //         background: "#FFF",
  //       },
  //     },
  //     category: {
  //       fontWeight: 400,
  //     },
  //     badge: {
  //       color: "#000",
  //       background: "#FFF",
  //     },
  //   },
  //   body: {
  //     background: "#F7F9FC",
  //   },
  //   footer: {
  //     color: grey[800],
  //   },
});
