import PropTypes from "prop-types";
// @mui
import { CssBaseline } from "@mui/material";
import {
  createTheme,
  ThemeProvider as MUIThemeProvider,
} from "@mui/material/styles";
// components
//
import palette from "./palette";
import { useMemo } from "react";

// ----------------------------------------------------------------------

ThemeProvider.propTypes = {
  children: PropTypes.node,
};

export default function ThemeProvider({ children }) {
  //   const { themeMode, themeDirection } = useSettingsContext();

  const themeOptions = useMemo(
    () => ({
      palette: palette("light"),
      // typography,
      // shape: { borderRadius: 8 },
      // direction: themeDirection,
      // shadows: shadows(themeMode),
      // customShadows: customShadows(themeMode),
    }),
    []
  );

  const theme = createTheme(themeOptions);

  //   theme.components = componentsOverride(theme);

  return (
    <MUIThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </MUIThemeProvider>
  );
}
