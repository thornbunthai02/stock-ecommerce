import { Suspense, lazy } from "react";
import LoadingScreen from "../components/loading-screen";

export const Loadable = (Component) => (props) =>
  (
    <Suspense fallback={<LoadingScreen />}>
      <Component {...props} />
    </Suspense>
  );

// auth component
export const Login = Loadable(lazy(() => import("../pages/auth/Login")));
export const Register = Loadable(lazy(() => import("../pages/auth/Register")));

// dashboard component
export const DashboardLayout = Loadable(
  lazy(() => import("../pages/dashboard-layout"))
);
export const Category = Loadable(lazy(() => import("../pages/category")));
export const Product = Loadable(lazy(() => import("../pages/product")));
export const DashboardPage = Loadable(lazy(() => import("../pages/dashboard")));
