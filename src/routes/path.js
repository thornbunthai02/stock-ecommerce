function path(root, sublink) {
  return `${root}${sublink}`;
}
// PATH
export const PATH = {
  dashboard: "/dashboard",
  auth: "/auth",
  login: "/login",
  register: "/register",
  main: "/main",
  category: "/category",
  product: "/product",
};
//
const DASHBOARD = PATH.dashboard;
const AUTH = PATH.auth;
export const AUTH_PATH = {
  root: AUTH,
  login: path(AUTH, PATH.login),
  register: path(AUTH, PATH.register),
};
//
export const DASHBOARD_PATH = {
  root: DASHBOARD,
  main: path(DASHBOARD, PATH.main),
  category: path(DASHBOARD, PATH.category),
  product: path(DASHBOARD, PATH.product),
};
