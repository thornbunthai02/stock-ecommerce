import { Navigate, createBrowserRouter } from "react-router-dom";
import { AUTH_PATH, DASHBOARD_PATH } from "./path";
import {
  Category,
  DashboardLayout,
  DashboardPage,
  Login,
  Product,
  Register,
} from "./element";

export const router = createBrowserRouter([
  // Auth path
  {
    path: "",
    element: <Navigate to={AUTH_PATH.login} replace />,
    index: true,
  },
  {
    path: AUTH_PATH.root,
    children: [
      {
        path: AUTH_PATH.login,
        element: <Login />,
      },
      {
        path: AUTH_PATH.register,
        element: <Register />,
      },
    ],
  },
  // Dashboard path
  {
    path: DASHBOARD_PATH.root,
    element: <DashboardLayout />,
    children: [
      {
        index: true,
        element: <Navigate to={DASHBOARD_PATH.main} replace />,
      },
      { path: DASHBOARD_PATH.main, element: <DashboardPage /> },
      { path: DASHBOARD_PATH.category, element: <Category /> },
      { path: DASHBOARD_PATH.product, element: <Product /> },
    ],
  },
]);
